在那房間裡，椅子和桌子都很高級。
但是，椅子只有兩張。桌子則是裏側一張，前側一張。

「還請稍等」

年輕職員這麼說了，只好站著等。

不久後，一位騎士抱著一疊文件過來，坐在裏側的椅子上。

「這位是迷宮事務統括官代理，騎士托羅古・斑恰拉大人」

騎士以視線指示了如此介紹的年輕職員。
年輕職員說道。

「還請坐下」
「就算說要坐，也只有一張椅子。誰來坐才好？」

雷肯說完，年輕職員以稍微困擾的表情看了騎士托羅古。

「由代表坐」
「雷肯。你坐吧」

被布魯斯卡這麼說，雷肯坐下後，騎士托羅古露出了笑容。

「首先，恭喜在百階層獲得勝利。以領主大人之名義，歡迎能在〈那一側〉戰鬥的新英雄的誕生」
「啊啊」
「你的名字是布魯斯卡，隊名是〈遊手好閒〉沒錯吧？」
「我叫雷肯。隊名是〈虹石〉」
「啊啊，聯合隊伍嗎。突破百階層時的編成是兩隊嗎？」
「對」
「告訴我兩個隊伍所有成員的名字吧」

騎士托羅古拿著紙筆說道。

「〈遊手好閒〉的成員是，布魯斯卡，秦格，喬安娜。〈虹石〉的成員是，雷肯、阿利歐斯」
「是以五人突破百階層的嗎」
「對」
「喔。五人是十五年以來了吧。那個，你是雷肯，阿利歐斯是誰。啊啊，你嗎。布魯斯卡是你對吧。秦格是你。妳是喬安娜。〈虹石〉的隊長是雷肯，〈遊手好閒〉的隊長是布魯斯卡，沒錯吧」
「啊啊」
「附有〈破損修復〉、〈體力吸收〉和〈劍速附加〉的彎刀，這還真是出了個好東西。難道是跟百階層的〈守護者〉戰鬥了嗎？」
「是有階梯的房間的〈鐵甲〉出的」
「是嗎。你們是貨真價實的呢。那麼，讓你們過來，是為了說明給予你們的幾個特權」
「特權？」
「對。首先，今後的鑑定費會是免費的」
「喔」
「今後希望別在一般的櫃台排隊，而是到這房間或是隔壁的房間。馬上就會有職員來應對」
「知道了」

雖然回答知道了，但雷肯完全沒打算接受免費鑑定。那是在暴露全部的本領，也討厭被籠絡。說到底，雷肯目前已經沒打算再接受鑑定了。

「藥和藥水能便宜購入。是一般的販賣所買不到的上級品」
「要在哪裡買？」
「跟待會帶路去的宿舍的接待訂購就好。武器、防具、裝飾品等也是，告訴接待購入希望的話，就會優先販售迷宮品，出了符合希望的新物品的時候也會聯絡」
「宿舍？」
「你們能免費住在待遇最好的宿舍裡。名叫〈錦嶺館〉的豪華設施。不管怎麼樣，五人都沒辦法探索更下方吧。住在〈錦嶺館〉的話，也方便接受聯合隊伍的幹旋」
「我很滿足於現在的宿屋。沒打算換宿屋」
「那就困擾了。得讓〈錦嶺館〉的職員記住你們的臉，也得跟你們說明〈錦嶺館〉的服務才行」
「雷肯」

布魯斯卡搭了話。

「嗯？」

雷肯轉過上半身看過去後，布魯斯卡說了讓人意外的話。

「對我們來說，去〈錦嶺館〉還太早了」
「什麼意思」

布魯斯卡在和秦格跟喬安娜相互遞了視線後回答。

「這次運氣好打倒了百階層的〈鐵甲白幽鬼〉。但那不是我們的實力。只是跟著你們〈虹石〉而已。我們還沒有住在〈錦嶺館〉的資格喔」

雷肯沒有否定這話語。因為這是事實。

「在百階層之前，跟其他隊伍戰鬥，再稍微提升點實力。之後的事會之後再考慮」
「呼嗯」

沒打算對〈遊手好閒〉的判斷表達異議。然後，覺得這判斷很賢明。

「受關照了。謝謝」
「怎麼會。這邊才是，謝謝你。然後阿，雷肯。要找能在百階層下側戰鬥的同伴，果然要在〈錦嶺館〉。我是為你好。最好先移居到那裡看看吧」
「這樣啊」

注視著對話的騎士托羅古插了嘴。

「了解了嗎。會祈願〈錦嶺館〉迎接〈遊手好閒〉的日子能早日到來。當然了，你們已經成功了百階層的魔獸討伐。隨時都能移居到〈錦嶺館〉。為此，也希望今天待會到〈錦嶺館〉露臉」

布魯斯卡回了同意給騎士托羅古，所以雷肯也決定陪同了。
此時收購金被送了過來。當場分攤了。
與騎士托羅古在那裡分別，在年輕職員的帶領上前往〈錦嶺館〉。

所幸，收購所離〈錦嶺館〉很近。

〈錦嶺館〉就在迷宮的北側。迷宮的入口在西南西，又在要繞路的位置，但離迷宮這麼近的話，往來會非常便利吧。
在〈錦嶺館〉受到了經理和其他重要員工的問候。

（一齊，而且角度相同地低下了頭）
（這種人在背叛的時候也會一齊背叛吧）

雷肯想著這些沒什麼根據的事。
雖然要馬上帶領到房間去，但雷肯今晚打算回〈拉芬的岩棚亭〉。

納可和妮露這對夫婦，知道雷肯等人今天要挑戰百階層的魔獸。現在應該正感到焦慮，等著報告才對。雖然感到焦慮的，主要是納可就是了。然後妮露想必在為慶功宴做準備。
所以今晚一定得回〈拉芬的岩棚亭〉才行。

「經理。目前的宿屋也放了行李，今晚不論如何都得回那邊去。明天會再過來」
「這樣啊。畢竟探索百階層以下的人，有規定要在本旅館住宿。會等待您的到來的」
「嗯？不在這裡住，就不能探索百階層以下嗎？」

應該沒有得知進入迷宮的冒険者跳到哪個階層的方法，也不覺得有不讓人進入特定階層的方法。要是有的話，會觸犯迷宮法才對。

「不不，當然沒那種事情。茨波魯特領主大人，會提供最好的待遇給優秀的冒険者們」
「原來如此。這倒是很清楚」

經理在笑臉上浮出了更多笑容。

雷肯等人回到了〈拉芬的岩棚亭〉。
然後盡情品嘗了勝利的美酒。