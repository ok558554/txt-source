（增加人數失敗了）
（等風頭過了再去斡旋所）
（目前只能憑兩人來進行攻略了）

說到底，靠兩人探索迷宮的八十階層帶並對付九隻敵人很奇怪，還好阿利歐斯不熟悉迷宮的常識。只要擺張理所當然的臉來講，應該就會那麼認為。在這意義上，剛剛把阿利歐斯帶到斡旋所是個失敗。但實際上也來到這裡了。再稍微拉高戰力，在戰法上下工夫的話，就能再前進一點。陷入僵局的時候再考慮對策就好。而且挑戰這迷宮的最大目的，是入手能安定地長期使用的劍。並沒有一定要踏破。

雷肯從〈收納〉取出〈澤娜的守護石〉並放進輕鎧的口袋裡，然後好好把口袋蓋緊。

（果然還是戴著這個吧）
（要是不捨得用擁有的東西而死去）
（或是讓迷宮探索停滯就太蠢了）

不使用〈澤娜的守護石〉能殲滅持有十之力的敵人的話，戴著〈澤娜的守護石〉就能打倒持有十二之力的敵人。從最初就依靠〈澤娜的守護石〉打倒持有十之力的敵人，就養成不了與持有十二之力的敵人戰鬥的力量。

儘管如此，戴著〈澤娜的守護石〉就能前進好幾階層的話，不戴著來戰鬥就很愚蠢。要怎麼兼顧，很讓人煩惱。
阿利歐斯很感興趣似地觀望著。阿利歐斯並不知道〈澤娜的守護石〉的事。雖然也有在尼納耶迷宮使用，但沒有特地說明。

「那麼，先進入這房間看看吧。我會先進去，但這次不會用〈雷擊〉。相對的，會用〈炎槍〉奪走一隻敵人的戰鬥力。走了」
「是」

雷肯拔出〈拉斯庫之劍〉，提煉魔力為〈炎槍〉做準備，大口吸氣並跳進房間。
進入的一瞬間，九隻魔獸看向這邊。

「〈炎槍〉！」

對從前方數來第二隻敵人擊出魔法，雙手握緊〈拉斯庫之劍〉突刺最前方敵人的喉嚨並削下頭部。
阿利歐斯突入了。
第二隻敵人被吹飛到後方，牽連後方兩隻〈黑肌〉並翻倒。
阿利歐斯穿過敵人之間突進，砍下了右方裡頭的〈黑肌〉的頭。

四隻〈赤肌〉果然在後方。到了七十階層帶，〈赤肌〉有時也會在前方，但到了八十階層帶，似乎會事先配置為後衛的樣子。
雷肯向前方移動，把起身的〈黑肌〉的右腳掌砍飛。〈澤娜的守護石〉的效果果然很高。昨天不論是腳掌還是手掌都砍不下來。

呻吟完的〈赤肌〉擊來了光之槍。雷肯把身體縮向眼前的〈黑肌〉。光之槍命中了〈黑肌〉的背。
在這期間，阿利歐斯砍下了兩隻〈赤肌〉的頭。

雷肯砍斷右前方的〈黑肌〉的左腳掌，繞到右方。雖然感受到中央的〈黑肌〉的斬擊命中了左手臂的衝擊，但仍無視並繞進右方。牽制著負傷的兩隻〈黑肌〉和在裡頭的無傷〈黑肌〉，同時砍下後衛〈赤肌〉的頭。

阿利歐斯此時砍下了最後的〈赤肌〉的頭。
剩下三隻〈黑肌〉。
雷肯和阿利歐斯保持著餘裕掃蕩了殘敵。

「只受到一發魔法就解決了。這戰法能行呢」
「啊啊。之前雖然用〈雷擊〉停住全體，但那效果也漸漸變弱了。這次用〈炎槍〉擊飛了前方第二隻〈黑肌〉，順利牽連了後方兩隻把他們弄倒。之後就好辦了」
「剛才的裝備，有提升攻擊力的效果嗎？」
「對」
「那個，是任何武器都會有效果嗎？」

一瞬，打算混淆答案，但既然都展現過效果了，就沒有隱藏的意義了。

「對。但是投擲武器就沒有效果吧」
「真是夢一般的裝備」
「去下一個吧」
「是」

下個房間出現了持有恩寵武器的魔獸。
是〈威力劍〉的上位版。
之後吃了午飯。

「雷肯殿。那個裝備，是不是有在尼納耶迷宮的最下層使用？」

真敏銳的傢伙。

「用了」
「果然。但是，當時的威力更特別呢。那個裝備，能調整威力嗎？」
「充分準備的話能使出只有一次的大效果。之後一陣子使不出大效果」
「嘿─」

之後，攻略了八十二階層的頭目房。
下午在四間房間戰鬥後，吃了晚飯，攻略八十三階層的頭目房並下到八十四階層，在迷宮裡睡了。恩寵品雖然出了四個，但都是就算對雷肯來說很稀奇，也不會動心的武器。

隔天下到了八十六階層，結束探索。
在宿屋用完餐後，阿利歐斯來訪了房間。

「有事嗎」
「請看看這個」

說著，交出了筆記。
〈立體知覺〉讀不了字。雷肯點了燈。

「〈光明〉」

九日 十一迷宮過夜
十日 十五〈十階層帶、二十階層帶〉
十一日 十二〈三十階層帶〉 迷宮過夜
十二日 十三〈四十階層帶〉
十三日 四〈五十階層帶〉 迷宮過夜
十四日 六〈六十階層帶〉
十五日 五迷宮過夜
十六日 五〈七十階層帶〉
十七日 休養日
十八日 三迷宮過夜
十九日 三
二十日 三迷宮過夜
二十一日 二〈八十階層帶〉
二十二日 休養日
二十三日 二迷宮過夜
二十四日 二

「日期後面的數字是什麼？啊啊，這樣啊。是那天攻略了幾階層吧」
「是的。看了這個有什麼想法？」
「休養日有兩天這麼多呢」
「那個感想不對。不是這點，攻略速度不會太快了嗎」
「是嗎？」
「那個阿。在尼納耶迷宮可是花了三十三天才攻略了四十五階層喔。就連那速度，也讓傑德先生很驚訝」
「是那樣嗎」
「就是那樣。但是在這裡才第五天就突入了五十階層帶。太亂來了。而且，中層和深層，會以更多人數來戰鬥對吧。在斡旋所沒被相信也是當然的」

（嘖）
（注意到了嗎）

「剛剛，是不是咋舌了？」
「沒有」
「嘛，這個迷宮，對雷肯殿和我來說，相性確實比尼納耶還要好。但是，同時對付八隻或九隻敵人果然很嚴苛。必須要更慎重地戰鬥。明天開始降低步調吧」

雷肯以和阿利歐斯有點不同的視點，看著攻略的紀錄。
那就是在迷宮過夜的頻率很低。

雷肯的想法是，進入了迷宮的話，最少也該持續探索個四、五天不出去。那是因為到外面去就會鬆懈。要維持高密度的戰鬥，果然還是別出去比較好。
但是這次，在最初的時候，一天踏破的階層數太多了，對手的難纏度變化得很激烈，所以暫且出去轉換心情反而有效果。

然後進入深層後，由於以兩人來對付許多敵人，所以消耗很大。儘管〈回復〉能在某種程度上消除肉體的疲勞，但不出去更新身體狀況，果然就無法充實下次戰鬥的氣力。
而且，這世界的迷宮，只要有〈印〉就能自由跳躍到任何階層，就算出去，也不太會有時間損失。

那麼乾脆每天出去如何呢。
不，這可不行。

阿利歐斯還沒習慣迷宮這東西。得兩天在迷宮過夜一次，把迷宮攻略的節奏刻入阿利歐斯的身體裡。
而且對雷肯自己來說，要是讓身體習慣了每天回宿屋這種戰法，在下個迷宮就會很辛苦。果然，每次進入迷宮，就有必要最少過夜一次。

「雷肯殿」
「怎樣？」
「能讓攻略的速度慢下來嗎？」
「說得也是啊」
「誒？」
「稍微慢一點前進吧。進幾個房間，充分鍛鍊這一階層的戰法後，再挑戰頭目房吧。怎麼了。一臉奇妙」
「不。雖然試著說了，但很意外能得到同意」
「你這傢伙還真複雜啊」

接下來，就別太急著前進吧。
在不會勉強的範圍內前進，就很充分了。

雖然說不定去不了最下層，但想去百階層之後。
那要花好幾個月吧。至今太順利了。
就當作拜之前很趕所賜而能有寬裕就好。